### NODE ###

FROM node:20.11-alpine AS build

WORKDIR /src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build --prod --base-href

### NGINX ###

FROM nginx:1.25.3-alpine

COPY --from=build /src/app/dist/angular-heroes/browser /usr/share/nginx/html

COPY --from=build /src/app/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80