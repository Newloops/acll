import {
  HttpEvent,
  HttpHandlerFn,
  HttpInterceptorFn,
  HttpRequest,
} from '@angular/common/http';
import { inject } from '@angular/core';
import { SpinnerService } from '@services/spinner.service';
import { Observable, delay, finalize } from 'rxjs';

export const SpinnerInterceptor: HttpInterceptorFn = (
  request: HttpRequest<unknown>,
  next: HttpHandlerFn
): Observable<HttpEvent<unknown>> => {
  const spinnerService = inject(SpinnerService);
  spinnerService.show();
  return next(request).pipe(
    delay(2000),
    finalize(() => spinnerService.hide())
  );
};
