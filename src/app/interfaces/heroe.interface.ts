export interface Heroe {
  id: number;
  name: string;
  owner: string;
}
