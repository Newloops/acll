import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Heroe } from '@interfaces/heroe.interface';
import { Observable, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HeroesService {
  private readonly httpClient = inject(HttpClient);
  private _heroes$ = new Subject<Heroe[]>();
  public heroes$ = this._heroes$.asObservable();

  constructor() {}

  public getHeroes(): Observable<Heroe[]> {
    return this.httpClient.get<Heroe[]>('/assets/json/heroes.json').pipe(
      tap((res: Heroe[]) => {
        localStorage.setItem('heroes', JSON.stringify(res));
        this._heroes$.next(res);
      })
    );
  }

  public setHeroes(heroe: Heroe[]) {
    localStorage.setItem('heroes', JSON.stringify(heroe));
    this._heroes$.next(heroe);
  }
}
