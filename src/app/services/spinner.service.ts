import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SpinnerService {
  private readonly _isLoadingSource$ = new BehaviorSubject<boolean>(false);
  public isLoading$ = this._isLoadingSource$.asObservable();

  public show(): void {
    this._isLoadingSource$.next(true);
  }

  public hide(): void {
    this._isLoadingSource$.next(false);
  }
}
