import { Injectable, inject } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { AlertComponent } from '@shared/dialog/alert/alert.component';
import { HeroeComponent } from '@shared/dialog/heroe/heroe.component';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  private readonly _dialog: MatDialog = inject(MatDialog);

  private static readonly DIALOG_DEFAULT_WIDTH = '512px';
  private readonly _defaultConfig: MatDialogConfig = {
    autoFocus: false,
    disableClose: false,
    hasBackdrop: true,
    role: 'dialog',
    width: DialogService.DIALOG_DEFAULT_WIDTH,
  };

  public openCreateAndEditHeroe(config: MatDialogConfig): MatDialogRef<any> {
    return this._dialog.open(HeroeComponent, {
      ...this._defaultConfig,
      ...config,
    });
  }

  public openAlert(config: MatDialogConfig): MatDialogRef<any> {
    console.log('openAlert');
    return this._dialog.open(AlertComponent, {
      ...this._defaultConfig,
      ...config,
    });
  }
}
