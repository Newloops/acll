import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutRoutingModule } from './layout-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule, LayoutRoutingModule, HttpClientModule],
  exports: [],
})
export class LayoutModule {}
