import { CommonModule } from '@angular/common';
import { Component, OnInit, ViewChild, inject } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { Heroe } from '@interfaces/heroe.interface';
import { DialogService } from '@services/dialog.service';
import { HeroesService } from '@services/heroes.service';
import { SpinnerService } from '@services/spinner.service';
import { SpinnerComponent } from '@shared/spinner/spinner.component';
import {
  BehaviorSubject,
  Observable,
  combineLatest,
  debounceTime,
  map,
  tap,
} from 'rxjs';

export interface PeriodicElement {
  name: string;
  id: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatIconModule,
    SpinnerComponent,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
  ],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  private readonly _heroesService = inject(HeroesService);
  private readonly _spinnerService = inject(SpinnerService);
  private readonly _dialogService = inject(DialogService);

  private _searchQuery$ = new BehaviorSubject<string>('');

  public displayedColumns: string[] = ['id', 'name', 'owner', 'actions'];
  public dataSource$ = new MatTableDataSource<Heroe>([]);

  public search: FormControl = new FormControl<string>('');
  public totalItems = 0;
  public pageSize = 5;
  public pageIndex = 0;

  ngOnInit(): void {
    this._comnineObservers()
      .pipe(
        tap((heroes) => {
          this.dataSource$.data = heroes;
          this.dataSource$.paginator = this.paginator;
        })
      )
      .subscribe();
    this._heroesService.getHeroes().subscribe();
    this._subscribeToSearch().subscribe();
  }

  public editHeroe(heroe: Heroe): void {
    this._dialogService
      .openCreateAndEditHeroe({
        data: {
          title: 'Editar heroe',
          heroe,
        },
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this._spinnerService.show();
          const heroes: Heroe[] = JSON.parse(localStorage.getItem('heroes')!);
          let foundIndex = heroes.findIndex((heroe) => heroe.id === result.id);
          heroes[foundIndex] = result;
          this._heroesService.setHeroes(heroes);
        }
      });
  }

  private _comnineObservers(): Observable<Heroe[]> {
    return combineLatest([
      this._searchQuery$,
      this._heroesService.heroes$,
    ]).pipe(
      tap(() => this._spinnerService.hide()),
      tap((items) => (this.totalItems = items?.length ? items?.length : 0)),
      map(([searchQuery, data]) =>
        data.filter((heroe) =>
          heroe.name.toLowerCase().includes(searchQuery.toLowerCase())
        )
      )
    );
  }

  public removeHeroe(heroe: Heroe): void {
    console.log('Paso por aqui');
    this._dialogService
      .openAlert({
        data: {
          title: 'Eliminar heroe',
          message: 'Estas seguro de eliminar a ',
          heroe,
        },
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          const heroes: Heroe[] = JSON.parse(localStorage.getItem('heroes')!);
          this._heroesService.setHeroes(
            heroes.filter((value) => value.id !== heroe.id)
          );
        }
      });
  }

  private _subscribeToSearch(): Observable<string> {
    return this.search.valueChanges.pipe(
      debounceTime(500),
      tap((value) => this._searchQuery$.next(value))
    );
  }
}
