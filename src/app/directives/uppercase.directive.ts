import { Directive, HostListener, inject } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  standalone: true,
  selector: 'input[uppercase]',
})
export class UppercaseDirective {
  private readonly ngControl = inject(NgControl);

  @HostListener('input', ['$event.target.value'])
  onInput(value: string) {
    this.ngControl.control?.setValue(value.toUpperCase());
  }
}
