import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink } from '@angular/router';
import { Heroe } from '@interfaces/heroe.interface';
import { DialogService } from '@services/dialog.service';
import { HeroesService } from '@services/heroes.service';
import { SpinnerService } from '@services/spinner.service';

@Component({
  selector: 'app-sidebar-menu',
  standalone: true,
  imports: [CommonModule, RouterLink, MatButtonModule],
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss'],
})
export class SidebarMenuComponent {
  private readonly _dialogService = inject(DialogService);
  private readonly _spinnerService = inject(SpinnerService);
  private readonly _heroesService = inject(HeroesService);

  public addHeroe(): void {
    this._dialogService
      .openCreateAndEditHeroe({
        data: {
          title: 'Añadir heroe',
        },
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this._spinnerService.show();
          const heroes: Heroe[] = JSON.parse(localStorage.getItem('heroes')!);
          const lastHeroe = heroes[heroes.length - 1];
          const newHeroe: Heroe = {
            id: lastHeroe!.id + 1,
            ...result,
          };
          heroes.push(newHeroe);
          this._heroesService.setHeroes(heroes);
        }
      });
  }
}
