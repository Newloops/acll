import { CommonModule } from '@angular/common';
import { Component, Inject, OnInit, inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { UppercaseDirective } from '@directives/uppercase.directive';
import { Heroe } from '@interfaces/heroe.interface';
import { Owner } from '@interfaces/owner.interface';

interface IForm {
  name: FormControl<string>;
  owner: FormControl<string>;
}

@Component({
  selector: 'app-dialog',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule,
    UppercaseDirective,
  ],
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.scss'],
})
export class HeroeComponent implements OnInit {
  private readonly _fb: FormBuilder = inject(FormBuilder);

  public form: FormGroup<IForm> = this._fb.nonNullable.group<IForm>({
    name: this._fb.nonNullable.control<string>(this.data.heroe?.name, [
      Validators.required,
      this.onDuplicate,
    ]),
    owner: this._fb.nonNullable.control<string>(
      this.data.heroe?.owner.toLowerCase(),
      [Validators.required]
    ),
  });
  public owners: Owner[] = [
    { value: 'marvel', viewValue: 'Marvel' },
    { value: 'dc', viewValue: 'DC' },
  ];

  constructor(
    private readonly _dialog: MatDialogRef<HeroeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {}

  public onSave(): void {
    let heroe;
    if (this.data?.heroe) {
      heroe = {
        id: this.data.heroe?.id,
        ...this.form.value,
      };
    } else {
      heroe = this.form.value;
    }
    if (this.form.valid) this._dialog.close(heroe);
  }

  public onCancel(): void {
    this._dialog.close(null);
  }

  public onDuplicate(control: AbstractControl) {
    const heroes: Heroe[] = JSON.parse(localStorage.getItem('heroes')!);
    const duplicateHeroe = heroes.find(
      (heroe) => heroe.name.toLowerCase() === control.value?.toLowerCase()
    );
    return duplicateHeroe ? { isDuplicate: true } : null;
  }
}
